# Deeplinks

#### Última actualización: 01/06/2018

Esta es la lista de los deeplinks que existen para iOS. La lista de los deeplinks de precies contiene todos los deeplinks. Para los demás existen deeplinks que contienen variables, por ejemplo, el deeplink para ir a la lista de skills por área está definido aqui como `http://kinedu.com/skills?area_id={area_id}` este deeplink para poder usarlo hay que sustituir `{area_id}` por el número de área que se quiere entrar. Para entrar al área física el deeplink quedaría: `http://kinedu.com/skills?area_id=1`.

Elementos con :exclamation: son deeplinks que no se han probado al momento de la última actualización.

## Prices

|Tiempo    | Deeplink inglés                      | Deeplink español                      |Precio  |Status |    |
|:--------:|:-------------------------------------|:--------------------------------------|:------:|:-----:|:---|
|Vitalicia |<http://kinedu.com/en/prices/lt1>     |<http://kinedu.com/es/prices/lt1>      |$ 109.99|:white_check_mark:|    |
|Vitalicia |<http://kinedu.com/en/prices/lt2>     |<http://kinedu.com/es/prices/lt2>      |$ 119.99|:white_check_mark:|    |
|Vitalicia |<http://kinedu.com/en/prices/lt3>     |<http://kinedu.com/es/prices/lt3>      |$ 129.99|:white_check_mark:|    |
|Vitalicia |<http://kinedu.com/en/prices/lt4>     |<http://kinedu.com/es/prices/lt4>      |$ 139.99|:white_check_mark:|    |
|Vitalicia |<http://kinedu.com/en/prices/lt5>     |<http://kinedu.com/es/prices/lt5>      |$ 149.99|:white_check_mark:|    |
|Anual     |<http://kinedu.com/en/prices/y_3999>  |<http://kinedu.com/es/prices/y_3999>   |$ 39.99 |:white_check_mark:|    |
|Anual     |<http://kinedu.com/en/prices/y_4999>  |<http://kinedu.com/es/prices/y_4999>   |$ 49.99 |:white_check_mark:|    |
|Anual     |<http://kinedu.com/en/prices/y_5999>  |<http://kinedu.com/es/prices/y_5999>   |$ 59.99 |:white_check_mark:|    |
|Anual     |<http://kinedu.com/en/prices/y_6999>  |<http://kinedu.com/es/prices/y_6999>   |$ 69.99 |:white_check_mark:|    |
|Anual     |<http://kinedu.com/en/prices/y_7999>  |<http://kinedu.com/es/prices/y_7999>   |$ 79.99 |:white_check_mark:|    |
|Anual     |<http://kinedu.com/en/prices/y>       |<http://kinedu.com/es/prices/y>        |$ 89.99 |:white_check_mark:|    |
|Semestre  |<http://kinedu.com/en/prices/s>       |<http://kinedu.com/es/prices/s>        |$ 49.99 |:white_check_mark:|    |
|Mensual   |<http://kinedu.com/en/prices/m>       |<http://kinedu.com/es/prices/m>        |$ 8.99  |:white_check_mark:|    |
|Mensual   |<http://kinedu.com/en/prices/m_499>   |<http://kinedu.com/es/prices/m_499>    |$ 4.99  |:white_check_mark:|    |
|Mensual   |<http://kinedu.com/en/prices/m_599>   |<http://kinedu.com/es/prices/m_599>    |$ 5.99  |:white_check_mark:|    |
|Mensual   |<http://kinedu.com/en/prices/m_699>   |<http://kinedu.com/es/prices/m_699>    |$ 6.99  |:white_check_mark:|    |
|Mensual   |<http://kinedu.com/en/prices/m_799>   |<http://kinedu.com/es/prices/m_799>    |$ 7.99  |:white_check_mark:|    |
|Mensual   |<http://kinedu.com/en/prices/m>       |<http://kinedu.com/es/prices/m>        |$ 8.99  |:white_check_mark:|    |
|Mensual   |<http://kinedu.com/en/prices/m_999>   |<http://kinedu.com/es/prices/m_999>    |$ 9.99  |:white_check_mark:|    |
|Mensual FT|<http://kinedu.com/en/prices/m_ft>    |<http://kinedu.com/es/prices/m_ft>     |$ 8.99  |:x:|Funciona el deeplink pero no está bien el porcentaje de descuento y el texto de ahorro.|
|Mensual FT|<http://kinedu.com/en/prices/m_ft_999>|<http://kinedu.com/es/prices/m_ft_999> |$ 9.99  |:x:|Funciona el deeplink pero no está bien el porcentaje de descuento y el texto de ahorro.|
|Semanal   |<http://kinedu.com/en/prices/w_0199>  |<http://kinedu.com/es/prices/w_0199>   |$ 1.99  |:white_check_mark:|    |
|Semanal   |<http://kinedu.com/en/prices/w_0249>  |<http://kinedu.com/es/prices/w_0249>   |$ 2.49  |:white_check_mark:|    |
|Semanal   |<http://kinedu.com/en/prices/w_0299>  |<http://kinedu.com/es/prices/w_0299>   |$ 2.99  |:white_check_mark:|    |

## Skills

|Nombre | Deeplink |Status |
|:-----:|:---------|:-----:|
|Home Skills|<http://kinedu.com/skills>|:white_check_mark:|
|Skills per Area|<http://kinedu.com/skills?area_id={area_id}>|:white_check_mark:|
|Detail of Skill|<http://kinedu.com/skills?skill_id={skill_id}&section=detail>|:exclamation:|
|Milestones per skill|<https://kinedu.com/skills?skill_id={skill_id}&section=milestones>|:exclamation:|
|How are my results calculated|<https://kinedu.com/skills?skill_id={skill_id}&section=results_info>|:exclamation:|
|Milestones learn more|<https://kinedu.com/skills?skill_id={skill_id}&milestone_id={milestone_id}&section=learn_more>|:exclamation:|
|Review assessment|<https://kinedu.com/skills?section=assessment>|:exclamation:|

## Catálogo

#### Catálogo prenatal
|Nombre | Deeplink |Status |
|:-----:|:---------|:-----:|
|Home Catálogo|<https://kinedu.com/catalog?is_prenatal=true>|:exclamation:|
|Artículos|<https://kinedu.com/catalog?section=articles&is_prenatal=true>|:exclamation:|
|Favoritos|<https://kinedu.com/catalog?section=favorites&is_prenatal=true>|:exclamation:|
|Filtro|<https://kinedu.com/catalog?section=filter&is_prenatal=true>|:exclamation:|
|Abrir artículo|<https://kinedu.com/catalog?section=open_article&article_id={article_id}&is_prenatal=true>|:exclamation:|

##### Catálogo (Faltan deeplinks)

|Nombre | Deeplink |Status |
|:-----:|:---------|:-----:|
|Home Catálogo|<https://kinedu.com/catalog>|:exclamation:|
|Artículos|<https://kinedu.com/catalog?section=articles>|:exclamation:|
|Home del catálogo con área seleccionada y para edad específica. 0 para all ages.|<https://kinedu.com/catalog?area_id=1&age=0>|:exclamation:|
|Artículos|<https://kinedu.com/catalog?section=articles>|:exclamation:|

## Milestones (Pendiente)

## Progreso (Pendiente)

## Activity Plan (Pendiente)

## Day Pass (Pendiente)

## Feedback (Pendiente)

## Family (Pendiente)
