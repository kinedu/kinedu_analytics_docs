# One Signal

|       |Tag                    | Valores              |        Notas                                                             |
:-------|:----------------------|:---------------------|:-------------------------------------------------------------------------|
|Familia|"main_family_type"     |"premium", "freemium" |                                                                          |
|Usuario|"id"                   |Número                |                                                                          |
|       |"last_version"         |String                |                                                                          |
|       |"user_name"            |String                |                                                                          |
|       |"user_email"           |email                 |                                                                          |
|       |"activity_count"       |Número                |                                                                          |
|       |"country"              |String                | Backend lo regresa con dos caracteres alfanuméricos.                     |
|       |"source"               |"NES", "KIN"          |                                                                          |
|       |"notifications_enabled"|"true", "False"       |                                                                          |
|Bebés  |"baby_1_isPregnancy"   |"true", "False"       |Se manda para el primer bebé de la familia.					              |
|       |"baby_1_age_in_weeks"  |"true", "False"       |Solo aplica para bebés prenatal.                                          |
|       |"baby_1_id"            |Número                |Se manda para el primer bebé de la familia.                               |
|       |"baby_1_name"          |String                |Se manda para el primer bebé de la familia.                               |
|       |"baby_1_age_in_months" |Número                |Se manda para el primer bebé de la familia.                               |
|       |"baby_1_birthday"      |Fecha                 |Se manda para el primer bebé de la familia. Fecha en formato `yyyy-MM-dd` |
|       |"baby_1_age_group"     |String ej. ("2-4")    |Se manda para el primer bebé de la familia.                               |
|       |"baby_1_skill_{id}_Progress"|Decimal   0-100 |Se manda para el primer vebé de la familia.                                |
|       |"baby_1_has_reminders" |"true", "False"       |Se manda para el primer vebé de la familia.                               |

Estos tags se mandan cada vez que el usuario inicia la aplicación.
Las del bebé, se borran antes de setearlas por si hay cambios en los bebés.
